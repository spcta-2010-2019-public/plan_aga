PlanAga::Application.routes.draw do
  devise_for :users, controllers: {
               sessions: 'sessions',
               registrations: 'admin/users/new',
            }

  # TinymceFm
  mount TinymceFm::Engine => "/tinymce_fm"

  # constraints :subdomain => 'admin' do
  namespace :admin do
    # scope :module => 'admin', :as => 'admin' do
      # defaults :subdomain => 'admin' do

        root :to => 'dashboard#index'
        resources :users do
          member do
            get :history
          end
        end
        resource :profile
        resources :aga_plans
        resources :aga_responsibles
        resources :aga_challenges do
          collection do
            # get 'general_info'
            # post 'save_general_info'
          end
          resources :aga_actions
        end
        resources :aga_informations
        resources :aga_timeline_items do
          member do
            get 'get_challenge_actions'
          end
        end
        resources :aga_documents
      # end
    # end
  end

  # constraints :subdomain => 'alianza' do
  #   scope :module => 'alianza', :as => 'alianza' do
  #     defaults :subdomain => 'alianza' do
        # root_path = 'aga_challenges#index'
        scope "(:plan)" do
          resources :documents, only: [:index]
          resources :aga_challenges, only: [:index] do
            resources :aga_actions, only: [:show]
            collection do
              get 'general'
              get 'information'
            end
          end
        end
        root :to => 'aga_challenges#index'
        get '/:plan', to: 'aga_challenges#index'
  #     end
  #   end
  # end

  # root :to => 'alianza/aga_challenges#index'
end
