class @ExpandUl
  
  constructor: (ul_class, number_to_show, expand_text) ->

    @uls = $("." + ul_class)
    @expand_text = expand_text
    @number_to_show = number_to_show

    @hide_lis()
    @add_expand_text()
      
  hide_lis: ->
    $.each(@uls, (i, ul) =>
      $(ul).find("li:gt(2)").hide()
    )

  add_expand_text: ->
    $.each(@uls, (i, ul) =>      
      lis_number = $(ul).find("li").length      
      if lis_number > @number_to_show
        $(ul).append(
          $("<li />")
            .text(@expand_text + " (#{lis_number - @number_to_show} más)")
            .attr("class", "more")
            .on "click", ->
              $(this)
                .closest("ul")
                .find("li")
                .fadeIn()
              $(this).remove()
        )
    )