$(document).ready ->
  $("#top .submenu-link").hover(
    (ev) -> $(this).find("ul").fadeIn(),
    (ev) -> $(this).find("ul").hide()
  )