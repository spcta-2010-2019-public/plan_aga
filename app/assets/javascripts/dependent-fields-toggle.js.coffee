class @DependentFieldsToggle

  constructor: (activator_selector, activator_value, toggable_selector, action = "show") ->

    $(activator_selector).change ->
      
      if $(this).val() + "" == activator_value + ""
        if action == "hide"
          $(toggable_selector).fadeOut "slow"
        else
          $(toggable_selector).fadeIn "slow"
      else
        if action == "hide"
          $(toggable_selector).fadeIn "slow"
        else
          $(toggable_selector).fadeOut "slow"

    if $(activator_selector + ":first").is("input")    
      $(activator_selector + ":checked").trigger("change")
    else
      $(activator_selector + ":selected").trigger("change")