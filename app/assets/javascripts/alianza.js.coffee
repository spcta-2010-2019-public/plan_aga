$(document).ready ->
  $('#challenges').chosen().change ->
    if ($(this).chosen().val() != '')
      $('.challenge').hide()
      $('#challenge'+$(this).chosen().val()).show()
    else
      $('.challenge').show()
  console.log($('.range-slider').attr("status"))
  $('.range-slider').jRange
    from: 0
    to: 100
    step: 1
    scale: [
      0
      25
      50
      75
      100
    ]
    format: '%s%'
    width: 300
    theme: $(this).attr("status")
    showLabels: true
    disable: true
  $('.bxslider').bxSlider
    adaptiveHeight: false
    mode: 'fade'
  $('.accordion-content').hide()
  $('p.accordion-summary-content').click ->
    $(this).hide()
    $(this).next('.accordion-content').not(':animated').slideDown().siblings '.accordion-content:visible'
    $(this).toggleClass 'current'
    return
  $('p.accordion-content').click ->
    $(this).slideUp()
    $(this).prev('.accordion-summary-content').show()
    $(this).siblings('p.accordion-content').removeClass 'current'
    return
