class @FiltrableTable
  
  constructor: (table_id, infos, container_id) ->
    
    @table = $ "#" + table_id
    @container = $ "#" + container_id
    @info = {}
    @labels = {}
    @selects = {}
    
    @css_classes = []
    @labels_text = []
    $.each infos, (css_class, label) =>
      @css_classes.push(css_class)
      @labels_text.push(label)
    
    @init_info()
    @collect_info()
    @create_selects()
    @create_labels()
    @append_prompt_to_selects()
    @append_options_to_selects()
    @add_event_to_selects()
    @place_selects()

  init_info: ->    
    $.each @css_classes, (i, val) =>
      @info[val] = []
    
  collect_info: ->
    $.each @css_classes, (i, val) =>
      $.each @table.find(".#{val}"), (j, element) =>
        @info[val].push($.trim($(element).text()))
        
  create_labels: ->
    $.each @labels_text, (i, text) =>
      @labels[i] = $("<label />").text("#{text}:")
          
  create_selects: ->
    $.each @css_classes, (i, val) =>
      @selects[val] = $("<select />").addClass("filtrable-table-filter")
      
  append_prompt_to_selects: ->
    $.each @selects, (i, select) =>
      ($("<option />").val("").text("-- Todos --")).appendTo($(select))
      
  place_selects: ->
    counter = 0
    $.each @selects, (i, select) =>      
      @container.append(
        $("<div />")
          .append(@labels[counter])
          .append($(select))
      )      
      counter++
      
  append_options_to_selects: ->
    $.each @selects, (i, select) =>
      $.each @info[i], (j, text) =>
        unless $(select).find("option[value='" + text + "']").length > 0
          $("<option />").val(text).text(text).appendTo($(select))
          
  add_event_to_selects: ->
    $.each @selects, (i, select) =>
      $(select).on "change", =>
        @table.find("tbody tr").hide()
        @collect_filters_values()
        selector_string = @create_filters_selector_string()
        
        @table.find("tbody tr#{selector_string}").show()
          
  collect_filters_values: ->
    @filters_values = []
    $.each $(".filtrable-table-filter"), (i, filter) =>
      @filters_values.push($(filter).val())
      
  create_filters_selector_string: ->
    string = ''
    for filter_value in @filters_values
      unless filter_value == ""
        string += ":contains('#{filter_value}')"
    string
      
      
  