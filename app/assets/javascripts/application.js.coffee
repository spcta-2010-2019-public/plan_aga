# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# the compiled file.
#
# WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
# GO AFTER THE REQUIRES BELOW.
#
#= require jquery
#= require jquery_ujs
#= require jquery.remotipart
#= require bootstrap.min
#= require lightslider.min
#= require lightgallery-all.min
#= require jquery.share
#= require rails.validations
#= require chosen-jquery
#= require jquery.livequery
#= require social-share-button
#= require cascade-dropdown
#= require fancybox
#= require jquery.livequery
#= require bootstrap-datetimepicker


$(document).ready ->

  # Chosen applied to each element with .chosen css class
  $(".chosen").chosen()

  # Cascade dropdowns
  $(".ajaxable").livequery ->
    new CascadeDropdown($(this).attr("id"), $(this).attr("target"), $(this).attr("extras"))

  # Each link with .fancybox css class will get the response in fancybox.
  # TODO change to live click
  #$("a.fancybox").fancybox
    #type: "ajax"

  $("a.fancybox").livequery ->
    $("a.fancybox").fancybox
      type: "ajax"

  $("a.single_image").fancybox()

  $("a.fancyinline").fancybox
    hideOnContentClick: true

  $("#lightSlider").lightSlider
    item:1,
    controls:false

  $("#tweets").lightSlider
    item:1,
    controls:false


  $('#aniimated-thumbnials').lightSlider
      gallery:true,
      item:6,
      loop:false,
      auto: true,
      thumbItem:0,
      slideMargin:0,
      enableDrag: false,
      currentPagerPosition:'left',
      onSliderLoad: (el) ->
        el.lightGallery selector: '#aniimated-thumbnials .lslide'
        return
  $('[data-toggle="tooltip"]').tooltip()

  $('#social-networks').share
    networks: ['facebook','twitter','tumblr','pinterest','googleplus']
    urlToShare: window.location.href
    theme: "square"
