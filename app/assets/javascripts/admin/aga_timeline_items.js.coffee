class @DropdownChallengeActions
  
  constructor: (dropdown_id, target_id, extras = null) ->
    
    @dropdown = $ "#" + dropdown_id    
    @target = $ "#" + target_id
    
    if extras == null
      @extras = {}
    else
      eval "this.extras = #{extras};"

    @dropdown.on "change", =>
      
      @clear_target()
      @append_prompt_to_target()
      
      unless @dropdown.val() == ""
        @build_url()
        @execute_ajax()
        
  build_url: -> 
    @url = "http://admin.gobiernoabierto.gob.sv/aga_timeline_items/#{@dropdown.val()}/#{@dropdown.attr('action')}.json?callback=?"
    #@url = "http://admin.localhost.com:3000/aga_timeline_items/#{@dropdown.val()}/#{@dropdown.attr('action')}.json?callback=?"
    #@url = "http://localhost:3000/api/dropdowns/#{@dropdown.val()}/#{@dropdown.attr('action')}.json"
    
  clear_target: ->
    @target
      .find("option")
      .remove()

  append_prompt_to_target: ->
    ($("<option />").text('-- Seleccione una opcion --').val('')).appendTo(@target)

  execute_ajax: =>
    $.getJSON @url, @extras, (data) =>
      $.each(data, (i, val) =>
        $("<option />")
          .val(i)
          .text(val)
          .appendTo(@target)
      )


$(document).ready ->
  $(".timelineable").livequery ->
    new DropdownChallengeActions($(this).attr("id"), $(this).attr("target"), $(this).attr("extras"))