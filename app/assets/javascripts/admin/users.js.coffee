$(document).ready ->
    
  if $("#user_change_password").is(":checked")
    $(".password").show()
  else
    $(".password").hide()
      
  $("#user_change_password").on "click", ->
    $(".password").fadeToggle();

  $("#permissions-list").on "change", ->
    permission = $(@).val()
    if permission
      jQuery.fancybox.showActivity();
      $('#user_role_attributes_role_id').val('')
      request = $.get '/permissions/add', (subject_class: permission, role_id: $(@).data('role-id'))
      request.success (data) -> jQuery.fancybox.hideActivity();
      

  $("a.close-permission").livequery ->
    $("a.close-permission").on "click", (e) ->
      e.preventDefault()
      e.stopPropagation()
      $(@).closest('.accordion-group').remove()

  $("#user_role_attributes_role_id").on "change", ->
    role_id = $(@).val()
    if role_id
      jQuery.fancybox.showActivity();
      $('.permissions-area').empty()
      $('#permissions-list').val('')
      request = $.get '/permissions/clone', (role_id: role_id)
      request.success (data) -> jQuery.fancybox.hideActivity();
