# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# the compiled file.
#
# WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
# GO AFTER THE REQUIRES BELOW.
#
#= require bootstrap-datetimepicker

$(document).ready ->
  if $("#attention-notifications").attr("data-info")
    notifications_data = $.parseJSON($("#attention-notifications").attr("data-info"))
    $.each notifications_data, (i, data) ->
      new AttentionIndicator(data.item_id, data.text, data.css_class)

  $("#save-and-new").on "click", ->
    form = $(this).closest("form")
    form.attr("action", form.attr("action") + "?saveandnew=true")
    form.submit()

  if $(".items-list input[type=checkbox]").length == 0
    $(".delete-controls").hide()
  else
    $(".delete-selected").attr("disabled", true)
    $(".delete-controls .check-all").on "click", ->
      $(".items-list input[type=checkbox]").prop("checked", true)
      $(".delete-selected").attr("disabled", false).addClass("btn-warning")
    $(".delete-controls .uncheck-all").on "click", ->
      $(".items-list input[type=checkbox]").prop("checked", false)
      $(".delete-selected").attr("disabled", true).removeClass("btn-warning")
    $(".items-list input[type=checkbox]").on "click", ->
      if $(".items-list input[type=checkbox]:checked").length > 0
        $(".delete-selected").attr("disabled", false).addClass("btn-warning")
      else
        $(".delete-selected").attr("disabled", true).removeClass("btn-warning")

  $("form.multi-delete").on "submit", ->
    return confirm("¿Realmente desea eliminar los registros seleccionados?")

  $("a.delete-all").on "click", ->
    return confirm("¿Realmente desea eliminar TODOS los registros?")
