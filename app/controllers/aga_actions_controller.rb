class AgaActionsController < AlianzaController
  before_filter :prepare_breadcrumb, :status
  respond_to :html

  def show
    @timeline = @challenge_action.aga_timeline_items.order("created_at DESC")
  end

  def index
    redirect_to alianza_aga_challenges_path and return
  end

  private

  def challenge
    @challenge = AgaChallenge.find(params[:aga_challenge_id])
  end
  helper_method :challenge

  def challenge_action
    @challenge_action = AgaAction.find(params[:id])
  end
  helper_method :challenge_action

  def prepare_breadcrumb
    add_breadcrumb "Inicio", alianza_aga_challenges_url
    add_breadcrumb challenge.title, alianza_aga_challenges_url
    add_breadcrumb challenge_action.title, alianza_aga_challenge_aga_action_url(challenge)
  end

  def status
    days = (@challenge_action.deadline - Date.current).to_i
    days = 0 if @challenge_action.percentage.to_i == 100
    @status = days >= 0 ? "theme-green" : (days < 0 && days >= -14) ? "theme-yellow" : "theme-red"
  end
  helper_method :status

end
