class SessionsController < Devise::SessionsController

  before_filter :set_layout

  def set_layout
    self.class.send("include", "AdminController::Filters".constantize)
    #init_namespace

    add_breadcrumb I18n.t("admin.layout.home"), send("admin_root_path")
    add_breadcrumb I18n.t('devise.sign_in')

    self.class.layout 'admin'
  end
end
