class DocumentsController < AlianzaController
  before_filter :prepare_breadcrumb
  layout 'alianza'

  def index
    @generals = AgaDocument.all
    @documents = @q.result(distinct: true)
  end

  def prepare_breadcrumb
    add_breadcrumb "Inicio", aga_challenges_url
    add_breadcrumb 'Documentos'
  end
end
