class AlianzaController < ApplicationController
  before_filter :prepare_search
  layout 'alianza'

  module Filters
    def get_pages
      #@pages = Page.where(portal: 'www', active: true, page_id: nil).order(:priority)
    end

    def init_namespace
      get_pages
    end
  end

  include Filters

  before_filter :init_namespace


  def default_url_options(options={})
    { :plan => params[:plan] || AgaPlan.current  }
  end

  def current_plan
    @current_plan ||= (params[:plan] ? AgaPlan.find(params[:plan]) : AgaPlan.current) rescue nil
  end
  helper_method :current_plan

  def actions_count
    @actions_count ||= current_plan.aga_challenges.map(&:aga_actions).flatten.size rescue nil
  end
  helper_method :actions_count

  def plans
    @plans ||= AgaPlan.order('title DESC') rescue nil
  end
  helper_method :plans

  def prepare_search
    @q = AgaTimelineItem.where('datafile_file_name IS NOT NULL').paginate(per_page: 10, page: params[:page]).ransack(params[:q])
  end
end
