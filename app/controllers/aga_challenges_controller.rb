# encoding: UTF-8
class AgaChallengesController < AlianzaController
  before_filter :average
  respond_to :html

  def index
    @challenges = current_plan.aga_challenges rescue nil
    @aga_information = current_plan.information rescue nil
  end

  def general

  end

  def information
    add_breadcrumb "Inicio", aga_challenges_url
    add_breadcrumb 'Información General'
  	@information = current_plan.information
  end

  def average
    @average ||= AgaAction.where("disabled = ? AND aga_challenge_id IN (?)", 0, current_plan.aga_challenge_ids).average(:percentage).to_f.round(2) rescue "0"
  end
  helper_method :average

end
