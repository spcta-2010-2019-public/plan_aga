class Admin::AgaChallengesController < AdminController

  def new
    @item = AgaChallenge.new
    @plans = AgaPlan.order('title DESC')
    render layout: false
  end

  def create
    @item = AgaChallenge.new
    set_attributes @item, params[:aga_challenge]
    @success = @item.save
    if @success
      flash.notice = "Se ha creado un nuevo desafio exitosamente"
    else
      @errors = @item.errors.messages.to_json.html_safe
      @plans = AgaPlan.order('title DESC')
    end
  end

  def edit
    @item = AgaChallenge.find(params[:id])
    @plans = AgaPlan.order('title DESC')
    render layout: false
  end

  def update

    @item = AgaChallenge.find(params[:id])
    set_attributes @item, params[:aga_challenge]
    @success = @item.save
    if @success
      flash.notice = "Se ha actualizado el desafio exitosamente"
    else
      @plans = AgaPlan.order('title DESC')
      @errors = @item.errors.messages.to_json.html_safe
    end
  end

  def destroy
    AgaChallenge.find(params[:id]).destroy
    flash[:notice] = t 'label.success_deleted'
    redirect_to admin_aga_challenges_url
  end

  def index
    @challenges = AgaChallenge.all
    @informations = AgaInformation.all
  end

  def general_info
    @aga_information ||= AgaInformation.count > 0 ? AgaInformation.first : AgaInformation.new
  end

  def save_general_info
    @aga_information ||= AgaInformation.count > 0 ? AgaInformation.first : AgaInformation.new
    set_information_attributes(@aga_information, params[:aga_information])
    if @aga_information.save
      flash! :success
      redirect_to admin_aga_challenges_url
    else
      render "general_info"
    end
  end

  def namespace
    :admin
  end
  helper_method :namespace

  def set_attributes (item, params)
    item.title = params[:title]
    item.description = params[:description]
    item.color = params[:color]
    item.aga_plan_id = params[:aga_plan_id]
  end

  def set_information_attributes(item, params)
    item.title = params[:title]
    item.institutions = params[:institutions]
    item.organizations = params[:organizations]
    item.institutions_description = params[:institutions_description]
    item.organizations_description = params[:organizations_description]
    item.general_info = params[:general_info]
    item.plan_description = params[:plan_description]
  end

end
