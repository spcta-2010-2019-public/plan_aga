class Admin::UsersController < Admin::TabledController

  def model
    User
  end

  def set_attributes (item, params)
    item.active = params[:active]
    item.email = params[:email]
    item.name = params[:name]
    item.role_attributes = params[:role_attributes] if params[:role_attributes]
    if params[:password].present?
      item.password = params[:password]
      item.password_confirmation = params[:password_confirmation]
    else
      unless item.encrypted_password.present?
        temporal_password = Devise.friendly_token.first(8)
        item.password = temporal_password
        item.password_confirmation = temporal_password
      end
    end
    item.user_id = current_user.try(:id) if item.user_id.nil?
  end

  def init_form
    # Build role if not present
    @item.build_role unless @item.role.present?
    # Load permissions
    @permissions = {}
    Permission.authorized_for(current_user).with_permissions_roles.each do |permission|
      @permissions[permission.name] ||= []
      @permissions[permission.name] << permission
    end
    Permission.authorized_for(current_user).each do |permission|
      @permissions[permission.name] ||= []
      unless @permissions[permission.name].include? permission
        @permissions[permission.name] << permission
      end
    end
    @permissions = @permissions.sort_by { |name, permissions| permissions.first.id }
    class_permissions = @item.permissions.map(&:subject_class).uniq
    @selected_permissions = Permission.authorized_for(current_user).where(subject_class: class_permissions).group_by{|p| p.name}.sort_by{|k,v| v.first.id}
    @my_permissions = @item.permissions.map(&:id)
  end

  def after_save
    if current_user.can?(:manage, 'all')
      nullify_permissions_roles_conditions
      save_permissions_roles
    end
  end

  def nullify_permissions_roles_conditions
    if params[:permission_ids].present?
      PermissionsRole.where(role_id: @item.role_id).where("permissions_roles.permission_id NOT IN(?)", Array.wrap(params[:permission_ids])).destroy_all
      Array.wrap(params[:permission_ids]).each do |permission_id|
        pr = PermissionsRole.where(role_id: @item.role_id, permission_id: permission_id).first_or_initialize
        pr.conditions = nil
        pr.super = false
        pr.save
      end
    else
      PermissionsRole.where(role_id: @item.role_id).destroy_all
    end
  end

  def save_permissions_roles
    # iterate over conditions params
    permissions_roles_params.each do |permission_role_param|
      permission = Permission.find permission_role_param[:permission_id]
      permissions = Permission.where subject_class: permission.subject_class
      options = { role_id: permission_role_param[:role_id], permission_id: permissions.map(&:id) }

      permission_roles = PermissionsRole.where(options)
      permission_roles.each do |permission_role|
        permission_role.conditions = permission_role_param[:conditions]
        permission_role.save
      end
    end
    # iterate over superuser permissions
    permissions_roles_superusers.each do |permission_role_superuser|
      permission = Permission.find permission_role_superuser[:permission_id]
      permissions = Permission.where subject_class: permission.subject_class
      options = { role_id: permission_role_superuser[:role_id], permission_id: permissions.map(&:id) }

      permission_roles = PermissionsRole.where(options)
      permission_roles.each do |permission_role|
        permission_role.super = permission_role_superuser[:super]
        permission_role.save
      end
    end
  end

  def permissions_roles_params
    permissions_roles_params = []
    params[:conditions] ||= {}

    params[:conditions].each do |permission_id, condition|
      permission_role_params = { role_id: @item.role_id, permission_id: permission_id }
      conditions = {}
      condition.each do |attribute, values|
        conditions[attribute.to_sym] = values
      end
      permission_role_params[:conditions] = conditions
      permissions_roles_params << permission_role_params
    end
    permissions_roles_params
  end

  def permissions_roles_superusers
    permissions_roles_superusers = []
    params[:superusers] ||= []

    params[:superusers].each do |permission_id|
      permissions_roles_superusers << { role_id: @item.role_id, permission_id: permission_id, super: true }
    end
    permissions_roles_superusers
  end

end
