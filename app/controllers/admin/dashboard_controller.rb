class Admin::DashboardController < AdminController

  # TODO, review why flashes are crazy

  include ActionView::Helpers::DateHelper

  before_filter :show_welcome, only: [:index]
  after_filter :discard_flash

  def index
    render 'admin/index'
  end

  private

    def show_welcome
      if flash[:notice]
        if current_user.last_sign_in_at.nil?
          flash[:info] = t('admin.index.first_welcome', name: current_user.name)
        else
          flash[:info] = t('admin.index.welcome', name: current_user.name, ago: time_ago_in_words(current_user.last_sign_in_at))
        end
        flash.delete(:notice)
      end
    end

    def discard_flash
      flash.discard
    end

end
