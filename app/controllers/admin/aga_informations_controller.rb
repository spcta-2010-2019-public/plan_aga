class Admin::AgaInformationsController < AdminController

  def edit
    @plans = AgaPlan.order('title DESC')
    @aga_information = AgaInformation.find params[:id]
  end

  def update
    @aga_information = AgaInformation.find params[:id]
    set_information_attributes(@aga_information, params[:aga_information])
    if @aga_information.save
      flash! :success
      redirect_to admin_aga_challenges_url
    else
      @plans = AgaPlan.order(title: :desc)
      render "edit"
    end
  end

  def new
    @plans = AgaPlan.order('title DESC')
    @aga_information = AgaInformation.new
  end

  def create
    @aga_information = AgaInformation.new
    set_information_attributes(@aga_information, params[:aga_information])
    if @aga_information.save
      flash! :success
      redirect_to admin_aga_challenges_url
    else
      @plans = AgaPlan.order('title DESC')
      render "new"
    end
  end

  def destroy
    AgaInformation.find(params[:id]).destroy
    flash[:notice] = t 'label.success_deleted'
    redirect_to admin_aga_challenges_url
  end



  def set_information_attributes(item, params)
    item.aga_plan_id = params[:aga_plan_id]
    item.title = params[:title]
    item.institutions = params[:institutions]
    item.organizations = params[:organizations]
    item.institutions_description = params[:institutions_description]
    item.organizations_description = params[:organizations_description]
    item.general_info = params[:general_info]
    item.plan_description = params[:plan_description]
  end

end
