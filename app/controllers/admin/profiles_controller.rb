class Admin::ProfilesController < Admin::TabledController

  def model
    User
  end

  def edit
    @item = current_user
  end

  def set_attributes (item, params)
    item.name = params[:name]
    item.email = params[:email]
    item.password = params[:password]
    item.password_confirmation = params[:password_confirmation]
    item.avatar = params[:avatar] if params[:avatar]
  end

  def update
    @item = current_user
    set_attributes @item, params[symbol]
    if @item.save
      sign_in current_user, bypass: true
      flash! :success
      redirect_to url_for(action: :edit)
    else
      render action: :edit
    end
  end
end
