class Admin::AgaTimelineItemsController < AdminController
  before_filter :load_challenges

  def new
    @item = AgaTimelineItem.new
  end

  def create
    @item = AgaTimelineItem.new
    set_attributes @item, params[:aga_timeline_item]
    @item.user_id = current_user.id
    @item.aga_action_id = params[:aga_action_id]
    if @item.save
      respond_to do |format|
        flash! :success
        format.html { redirect_to admin_aga_timeline_items_url }
      end
    else
      respond_to do |format|
        format.html { render "new" }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @item = AgaTimelineItem.where(id: params[:id]).first
    @aga_actions = @item.aga_action.aga_challenge.aga_actions rescue []
  end

  def update
    @item = AgaTimelineItem.find(params[:id])
    set_attributes @item, params[:aga_timeline_item]
    if @item.save
      respond_to do |format|
        flash! :success
        format.html { redirect_to admin_aga_timeline_items_url }
      end
    else
      respond_to do |format|
        format.html { render "edit" }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    AgaTimelineItem.where(id: params[:id], user_id: current_user.id).first.destroy
    flash[:notice] = t 'label.success_deleted'
    redirect_to action: :index
  end

  def index
    @aga_comments = AgaTimelineItem.order("created_at DESC") rescue ""
  end

  def get_challenge_actions
    @challenge = AgaChallenge.includes(:aga_actions).find(params[:id])
    @aga_actions = {}

    @challenge.aga_actions.each do |c|
      @aga_actions[c.id] = c.title.to_s
    end

    respond_to do |format|
      format.json { render json: @aga_actions, callback: params[:callback] }
    end

  end

  def set_attributes (item, params)
    item.title = params[:title]
    item.description = params[:description]
    item.datafile = params[:datafile] if params[:datafile].present?
    item.aga_responsible_id = params[:aga_responsible_id]
  end

  def namespace
    :admin
  end
  helper_method :namespace

  def model
    AgaTimelineItem
  end

  private

  def load_challenges
    @challenges = AgaChallenge.includes(:aga_actions).order('aga_challenges.aga_plan_id DESC, aga_challenges.title ASC')
    @responsibles = AgaResponsible.order('name ASC')
  end

end
