# A base class for controllers which display their data in a table.
#
# Your control has to extend this class and must overwrite the following methods:
#   - #model
#
# You should override:
#   - #set_attributes
#   - #edit_item_path
#   - #item_path
#   - #namespace
#   - #headline
#
# You may override:
#   - #init_form
#
# Furthermore you need 3 partials in your controllers view folder:
#   - _form.html.haml: A form for your model ordered in divs line by line.
#   - _table_header.html.haml: A th element for every attribute that your model partial returns in the same order. They
#       must on the top level, this means there must not be a tr element around them.
#   - model partial (e.g. _dish_type.html.haml): This should display all attributes as td elements on the top level.
#       This means that they must not be surrounded by a tr element.
class Admin::TabledController < AdminController

  include DateHelper

  # CanCan authorization
  #
  hide_action :editable_attributes,
    :model,
    :set_attributes,
    :init_form,
    :edit_item_path,
    :history_item_path,
    :item_path,
    :namespace,
    :headline,
    :symbol,
    :render_form,
    :menu_permissions,
    :render_404,
    :tinymce_fm_settings,
    :tinymce_fm_settings?,
    :tinymce_fm_settings=,
    :tinymce_filemanager_list_images,
    :tinymce_filemanager_upload_image,
    :tinymce_filemanager_destroy_image,
    :tinymce_filemanager_create_images_folder,
    :tinymce_filemanager_list_media,
    :tinymce_filemanager_upload_media,
    :tinymce_filemanager_destroy_media,
    :tinymce_filemanager_create_media_folder,
    :tinymce_filemanager_list_links,
    :images_folder,
    :media_folder,
    :has_csv_import?,
    :csv_import_settings,
    :csv_import_options,
    :init_filters,
    :after_save,
    :after_create,
    :after_update,
    :init_namespace,
    :validated_date

  # This is raised if you do not overwrite #model to return the handled model type.
  class NoTypeGivenError < Exception

  end

  # Gets the model type that this class should work on.
  #
  # You must overwrite this in a derived class.
  #
  # Examples
  #
  #   def model_type
  #     User
  #   end
  #
  # Returns a class object that acts like an active record model.
  def model
    raise NoTypeGivenError
  end

  helper_method :model

  # Reads the values from params and sets them on item.
  #
  # You should overwrite this in a derived class with a whitelist of settable attributes.
  #
  # Returns nothing.
  def set_attributes (item, params)

  end

  def has_csv_import?
    csv_import_settings.any?
  end

  helper_method :has_csv_import?

  def csv_import_settings
    {}
  end

  helper_method :csv_import_settings

  def csv_import_options
    {}
  end

  helper_method :csv_import_options

  def csv_import
    @csv_import = CsvImport.new(params[:csv_import])
    @csv_import.user = current_user

    if @csv_import.save
      if @csv_import.import model, csv_import_settings, csv_import_options
        redirect_to url_for(action: :index), flash: :success
      else
        @csv_import.destroy
        init_form
        render_form model.new, t('label.new_entry')
      end
    else
      init_form
      render_form model.new, t('label.new_entry')
    end
  end

  def csv_example
    if has_csv_import?
      items = model.limit(15)

      csv_string = CSV.generate(force_quotes: true, col_sep: ';', encoding: 'UTF-8') do |csv|
        csv << csv_import_settings.keys.map{ |key| I18n.t("activerecord.attributes.#{model.to_s.underscore}.#{key}") }
        items.each do |item|
          csv << csv_import_settings.keys.map do |key|
            if item.read_attribute(key).is_a? TrueClass
              1
            elsif item.read_attribute(key).is_a? FalseClass
              0
            else
              item.read_attribute(key)
            end
          end
        end
      end

      respond_to do |format|
        format.csv {
          send_data csv_string,
                    type: 'text/csv; charset=utf-8; header=present',
                    filename: (I18n.t("activerecord.controllers.#{model.to_s.underscore.pluralize}") + '.csv')
        }
      end
    end
  end

  # Initialize any extra variables that are used in the form partial.
  #
  # You may override this in a derived class.
  #
  # Extra variables are variables like models from the DB to choose from in a select tag. The partial object will be
  # initialized automatically.
  #
  # Returns nothing.
  def init_form

  end

  def editable_attributes
    { }
  end

  helper_method :editable_attributes

  # You should overwrite this in a derived class.
  #
  # Examples
  #
  #   def edit_item_path (item)
  #     edit_user_path item
  #   end
  #
  # Returns the path for editing item as a string.
  def edit_item_path (item)
    send("edit_#{namespace}_#{model.to_s.underscore}_path", item)
  end

  helper_method :edit_item_path

  def history_item_path (item)
    send("history_#{namespace}_#{model.to_s.underscore}_path", item)
  end

  helper_method :history_item_path


  # You should overwrite this in a derived class.
  #
  # Examples
  #
  #   def item_path (item)
  #     user_path item
  #   end
  #
  # Returns the path to item as a string.
  def item_path (item)
    send("#{namespace}_#{model.to_s.underscore}_path", item)
  end

  helper_method :item_path

  # You should overwrite this in a derived class.
  #
  # Returns the symbol for the derived controller's namespace or nil if it is in the top namespace.
  def namespace
    :admin
  end

  helper_method :namespace

  # You should overwrite this in a derived class.
  #
  # Returns the headline that should be displayed on the index page as a string.
  def headline
    t("activerecord.controllers.#{model.to_s.pluralize.underscore}")
  end

  helper_method :headline

  # Translates the given #model into it's corresponding symbol.
  #
  # Examples
  #
  #   # #model => CamelCasedType
  #   symbol
  #   # => :camel_cased_type
  #
  # Returns the symbol for #model.
  def symbol
    model.name.underscore.to_sym
  end

  def init_filters

  end

  # Loads all items of #model.
  #
  # Returns nothing.
  def index
    page = params[:page].to_i
    page = 1 if page < 1

    @q = model.order(:name).search(params[:q])
    @items = @q.result.paginate(page: params[:page])

    init_filters
  end

  # Creates a new instance of #model.
  #
  # Returns nothing.
  def new
    if has_csv_import?
      @csv_import = CsvImport.new
    end
    @item = model.new
    render_form @item, t('label.new_entry')
  end

  def after_save

  end

  def after_create

  end

  def after_update

  end

  # Creates a new instance of #model from request parameters.
  #
  # Returns nothing.
  def create

    @item = model.new
    set_attributes @item, params[symbol]

    if @item.save
      after_create
      after_save
      respond_to do |format|
        flash! :success
        if params[:saveandnew].present?
          format.html { redirect_to(url_for(action: :new)) }
        else
          format.html { redirect_to(url_for(action: :edit, id: @item.id)) }
        end
        format.json { render json: @item }
      end
    else
      if has_csv_import?
        @csv_import = CsvImport.new
      end
      respond_to do |format|
        format.html { render_form @item, t('label.new_entry') }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # Renders the form for
  #
  # Returns nothing.
  def render_form (item, header)
    @item = item
    @header = header

    init_form

    render 'admin/tabled/edit'
  end

  # Destroys the #model with id :id.
  #
  # Returns nothing.
  def destroy
    model.destroy params[:id]

    flash[:notice] = t 'label.success_deleted'
    redirect_to action: :index
  end

  # Loads the #model with id :id for editing.
  #
  # Returns nothing.
  def edit
    @item = model.find(params[:id])
    render_form @item, t('label.edit_entry')
  end

  # Updates the #model with id :id from request parameters.
  #
  # Returns nothing.
  def update
    @item = model.find params[:id]

    set_attributes @item, params[symbol]

    if @item.save
      after_update
      after_save

      flash! :success
      respond_to do |format|
        if params[:saveandnew].present?
          format.html { redirect_to url_for(action: :new) }
        else
          format.html { redirect_to url_for(action: :edit) }
        end
        format.json do
          render json: @item, head: :no_content
        end
      end
    else
      respond_to do |format|
        format.html { render_form @item, t('label.edit_entry') }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def history
    @item = model.find params[:id]
  end

  def multi_delete
    items = model.where id: params[:ids]
    items.each do |item|

      history = "#{model}History".constantize.new
      model.columns.each do |column|
        if column.name != 'id' && history.respond_to?("#{column.name}=") && item.respond_to?(column.name)
          history.send("#{column.name}=", item.send(column.name))
        end
      end
      history.deleted_at = Time.current
      history.save
      item.destroy
    end

    flash[:notice] = t 'label.success_deleted'
    redirect_to action: :index
  end

  def delete_all
    items = model
    items.each do |item|

      history = "#{model}History".constantize.new
      model.columns.each do |column|
        if column.name != 'id' && history.respond_to?("#{column.name}=") && item.respond_to?(column.name)
          history.send("#{column.name}=", item.send(column.name))
        end
      end
      history.deleted_at = Time.current
      history.save
      item.destroy
    end

    flash[:notice] = t 'label.success_deleted'
    redirect_to action: :index
  end

  # Permission helper method, view permissions rake task
  #
  # Returns array
  def permission
    [model, I18n.t("activerecord.controllers.#{model.to_s.pluralize.underscore}").capitalize]
  end

  helper_method :permission

end
