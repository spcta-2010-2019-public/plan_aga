class Admin::AgaPlansController < Admin::TabledController

  def model
    AgaPlan
  end

  def set_attributes (item, params)
    item.title = params[:title]
    item.datafile = params[:datafile] if params[:datafile].present?
  end

  def init_form
  end

  # Loads all items of #model.
  #
  # Returns nothing.
  def index
    page = params[:page].to_i
    page = 1 if page < 1

    @q = model.order(:title).search(params[:q])
    @items = @q.result.paginate(page: params[:page])

    init_filters
  end

  # Updates the #model with id :id from request parameters.
  #
  # Returns nothing.
  def update
    @item = model.find params[:id]

    set_attributes @item, params[symbol]

    if @item.save
      after_update
      after_save

      flash! :success
      respond_to do |format|
        if params[:saveandnew].present?
          format.html { redirect_to url_for(action: :new) }
        else
          format.html { redirect_to edit_admin_aga_plan_url(@item) }
        end
        format.json do
          render json: @item, head: :no_content
        end
      end
    else
      respond_to do |format|
        format.html { render_form @item, t('label.edit_entry') }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

end
