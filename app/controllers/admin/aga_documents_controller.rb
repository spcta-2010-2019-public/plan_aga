class Admin::AgaDocumentsController < AdminController

  def new
    @item = AgaDocument.new
  end

  def create
    @item = AgaDocument.new
    set_attributes @item, params[:aga_document]
    @item.user_id = current_user.id
    if @item.save
      respond_to do |format|
        flash! :success
        format.html { redirect_to admin_aga_documents_url }
      end
    else
      respond_to do |format|
        format.html { render "new" }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @item = AgaDocument.where(id: params[:id], user_id: current_user.id).first
  end

  def update
    @item = AgaDocument.find(params[:id])
    set_attributes @item, params[:aga_document]
    if @item.save
      respond_to do |format|
        flash! :success
        format.html { redirect_to admin_aga_documents_url }
      end
    else
      respond_to do |format|
        format.html { render "edit" }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    AgaDocument.where(id: params[:id], user_id: current_user.id).first.destroy
    flash[:notice] = t 'label.success_deleted'
    redirect_to action: :index
  end

  def index
    @aga_documents = AgaDocument.order("created_at DESC") rescue ""
=begin
    if current_user.can? :manage, :all
      @aga_documents = AgaDocument.order("created_at DESC") rescue ""
    else
      @aga_documents = AgaDocument.where(user_id: current_user.id).order("created_at DESC") rescue ""
    end
=end
  end

  def set_attributes (item, params)
    item.title = params[:title]
    item.description = params[:description]
    item.datafile = params[:datafile] if params[:datafile].present?
  end

  def namespace
    :admin
  end
  helper_method :namespace

end
