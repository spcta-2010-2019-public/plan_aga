class AgaInformation < ActiveRecord::Base
  attr_accessible :aga_plan_id, :title, :institutions, :organizations, :institutions_description, :organizations_description,
                  :general_info, :plan_description

  validates :aga_plan_id, presence: true, uniqueness: true

    belongs_to :aga_plan
end
