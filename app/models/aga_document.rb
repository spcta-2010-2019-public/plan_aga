class AgaDocument < ActiveRecord::Base
  attr_accessible :title, :description

  belongs_to :user

  attr_accessible :datafile
  has_attached_file :datafile

  validates :title, presence: true
  validates_length_of :description, :maximum => 250, :too_long => "Debe contener un maximo de 250 caracteres."

end
