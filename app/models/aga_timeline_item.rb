class AgaTimelineItem < ActiveRecord::Base
  attr_accessible :title, :description, :aga_responsible_id

  belongs_to :aga_action
  belongs_to :aga_responsible
  belongs_to :user

  attr_accessible :datafile
  has_attached_file :datafile

  validates :title, :description, :aga_action_id, presence: true
  validates_length_of :description, :maximum => 250, :too_long => "Debe contener un maximo de 250 caracteres."

  SUPPORTED_IMAGE_FORMATS = [
    "image/jpeg",
    "image/jpg",
    "image/png",
    "image/gif",
    "image/bmp",
    "image/tiff",
    "image/jfif",
  ]
  SUPPORTED_IMAGES_REGEX = Regexp.new('\A(' + SUPPORTED_IMAGE_FORMATS.join('|') + ')\Z')

  def image?
    (datafile_content_type =~ SUPPORTED_IMAGES_REGEX).present?
  end

  def full_root
    [
      [
        (aga_action.aga_challenge.aga_plan.title rescue ''),
        (aga_action.aga_challenge.title rescue '')
      ].join(" "),
      aga_action.title
    ].join("\r\n")
  end

end
