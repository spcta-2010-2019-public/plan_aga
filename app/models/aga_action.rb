class AgaAction < ActiveRecord::Base
  attr_accessible :title, :description, :percentage, :institutions, :managers, :disabled, :deadline, :aga_indicators_attributes

  belongs_to :aga_challenge
  has_many :aga_indicators, dependent: :destroy
  has_many :aga_timeline_items

  extend FriendlyId
  friendly_id :title, :use => :slugged

  accepts_nested_attributes_for :aga_indicators, allow_destroy: true, reject_if: lambda { |t| t['title'].nil? }

  validates :title, :description, :percentage, :institutions, :managers, :deadline, presence: true
  validates :percentage, :numericality => { :less_than_or_equal_to => 100 }


  def name
    ""
  end

  
end
