class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable

  devise  :database_authenticatable,
          # :registerable,
          :recoverable,
          :rememberable,
          :trackable,
          # :validatable,
          :token_authenticatable
          # :omniauthable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me

  # Validations
  validates :email, :presence => true, :uniqueness => { :case_sensitive => false }, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
  validates :name, :presence => true #:role_id
  validates :password, :presence => true, :confirmation => true, :on => :create
  validates :password, :presence => true, :confirmation => true, :if => Proc.new{|o| o.password.present?}

  # Paperclip
  has_attached_file :avatar, styles: { thumb: "32", mini: "40x40#", small: "60" }

  # Callbacks
  before_save :ensure_authentication_token

  # Will paginate uses this method
  # to know how many rows to show
  #
  def self.per_page
    50
  end

  def name
    return email if read_attribute(:name).blank?
    read_attribute(:name)
  end

  def name_with_email
    return email if read_attribute(:name).blank?
    [read_attribute(:name), "(#{email})"].join(' ')
  end

  def self.new_with_session(params, session)
    if session['devise.user_attributes']
      new(session['devise.user_attributes'], without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end


end
