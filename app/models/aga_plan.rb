class AgaPlan < ActiveRecord::Base
  attr_accessible :title


  attr_accessible :datafile
  has_attached_file :datafile

  has_many :aga_challenges, dependent: :destroy
  has_many :aga_informations, dependent: :destroy

  extend FriendlyId
  friendly_id :title, :use => :slugged

  def self.current
    order('title DESC').limit(1).first
  end

  def information
    aga_informations.first
  end

  def name
    self[:title]
  end
end
