class AgaChallenge < ActiveRecord::Base
  attr_accessible :aga_plan_id, :title, :color, :description

  has_many :aga_actions, dependent: :destroy
  belongs_to :aga_plan

  extend FriendlyId
  friendly_id :title, :use => :slugged

  validates :title, :description, :aga_plan_id, presence: true

  def full_title
    [aga_plan.title, title].join(' ')
  end

end
