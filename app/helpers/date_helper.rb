module DateHelper

  def validated_date symbol, name
    args = []
    (1..3).each do |i|
      return nil if params[symbol]["#{name}(#{i}i)"].blank?
      args << params[symbol]["#{name}(#{i}i)"].to_i
    end
    Date.new args[0], args[1], args[2]
  end

  def validated_datetime symbol, name
    args = []
    (1..5).each do |i|
      return nil if params[symbol]["#{name}(#{i}i)"].blank?
      args << params[symbol]["#{name}(#{i}i)"].to_i
    end
    Time.zone.local args[0], args[1], args[2], args[3], args[4]
  end
end
