source 'https://rubygems.org'

gem 'rails', '3.2.8'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

# gem 'mysql2', '0.3.17'
gem 'pg'

gem 'devise', '~> 2.1.2'


# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '>= 1.0.3'
end

gem 'jquery-rails'

gem 'haml-rails'

gem "therubyracer"
gem "less-rails" #Sprockets (what Rails 3.1 uses for its asset pipeline) supports LESS

gem 'twitter-bootstrap-rails'
gem 'will_paginate', '~> 3.0'

# Fix for paginations bootstrap styles
gem 'bootstrap-will_paginate'

# Gem to show error messages using javascript
gem 'client_side_validations'

# Standardizing locales for flash messages
gem 'bootstrap_flash_messages'

# Nice look for confirm dialogs
gem 'twitter-bootstrap-rails-confirm'

# Library for making long, unwieldy select boxes more user friendly.
gem 'chosen-rails'


# Use FancyBox with Rails
gem 'fancybox-rails'

# Easy file attachment management for ActiveRecord
gem 'paperclip'

# Easy enum for models, objects and views
gem 'enumerated_attribute'

# TinyMCE editor with assets manager
gem 'mini_magick'
gem 'tinymce_fm'

# Rails plugin to conveniently handle multiple models in a single form
gem 'nested_form'

# Object-based searching. The successor to MetaSearch.
gem 'ransack'

# Date and time validation plugin for ActiveModel and Rails.
gem 'validates_timeliness', '~> 3.0'

# A Datepicker for Twitter Bootstrap, integrated with Rails assets pipeline
gem 'bootstrap-datetimepicker-rails'
# jquery ui
gem 'jquery-ui-rails'

# Helper for add social share feature in your Rails app. Twitter, Facebook, Weibo, Douban, QQ ...
gem 'social-share-button'

# Allows you to create pretty URLs and work with human-friendly strings
gem 'friendly_id'


# ajax upload files
gem 'remotipart'

# for zip compression
gem 'rubyzip'

# Database based asynchronous priority queue system
gem 'daemons'
gem 'delayed_job_active_record'

# for geolocalize
gem 'geokit'
gem 'geokit-rails'

# for errors notification by mail
gem 'exception_notification'

# SOAP client
gem 'savon'

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'debugger'
