class CreateAgaDocuments < ActiveRecord::Migration
  def change
    create_table :aga_documents do |t|
      t.belongs_to :user
      t.string :title
      t.text :description
      t.attachment :datafile
      t.timestamps
    end
  end
end
