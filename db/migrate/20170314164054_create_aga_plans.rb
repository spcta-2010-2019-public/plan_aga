class CreateAgaPlans < ActiveRecord::Migration
  def change
    create_table :aga_plans do |t|
      t.string :title
      t.string :slug
      t.timestamps
    end
    add_index :aga_plans, :slug
    add_column :aga_challenges, :aga_plan_id, :integer
    add_index :aga_challenges, :aga_plan_id
    add_column :aga_informations, :aga_plan_id, :integer
    add_index :aga_informations, :aga_plan_id
  end
end
