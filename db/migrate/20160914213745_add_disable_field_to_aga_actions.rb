class AddDisableFieldToAgaActions < ActiveRecord::Migration
  def change
    add_column :aga_actions, :disabled, :integer, default: 0
  end
end
