class AddAssetFieldToAgaPlansTable < ActiveRecord::Migration
  def change
    add_attachment :aga_plans, :datafile
  end
end
