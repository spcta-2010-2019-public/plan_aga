class CreateAgaInformations < ActiveRecord::Migration
  def change
    create_table :aga_informations do |t|
      t.string :title
      t.integer :institutions
      t.integer :organizations
      t.text :institutions_description
      t.text :organizations_description
      t.text :general_info
      t.text :plan_description
      t.timestamps
    end
  end
end
