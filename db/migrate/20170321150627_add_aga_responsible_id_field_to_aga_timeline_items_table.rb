class AddAgaResponsibleIdFieldToAgaTimelineItemsTable < ActiveRecord::Migration
  def change
    add_column :aga_timeline_items, :aga_responsible_id, :integer
    add_index :aga_timeline_items, :aga_responsible_id
  end
end
