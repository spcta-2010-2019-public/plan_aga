class CreateAgaChallenges < ActiveRecord::Migration
  def change
    create_table :aga_challenges do |t|
      t.string :title
      t.string :color, default: "#2D2F3C"
      t.string :slug
      t.timestamps
    end
  end
end
