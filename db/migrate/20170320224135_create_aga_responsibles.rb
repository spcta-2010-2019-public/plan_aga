class CreateAgaResponsibles < ActiveRecord::Migration
  def change
    create_table :aga_responsibles do |t|
      t.string :name
      t.timestamps
    end
  end
end
