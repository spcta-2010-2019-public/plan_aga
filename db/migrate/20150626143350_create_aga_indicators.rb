class CreateAgaIndicators < ActiveRecord::Migration
  def change
    create_table :aga_indicators do |t|
      t.belongs_to :aga_action
      t.string :title
      t.timestamps
    end
  end
end
