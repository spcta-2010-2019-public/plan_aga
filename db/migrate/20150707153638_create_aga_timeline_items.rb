class CreateAgaTimelineItems < ActiveRecord::Migration
  def change
    create_table :aga_timeline_items do |t|
      t.belongs_to :aga_action
      t.belongs_to :user
      t.string :title
      t.text :description
      t.attachment :datafile
      t.timestamps
    end
  end
end
