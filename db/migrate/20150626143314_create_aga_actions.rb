class CreateAgaActions < ActiveRecord::Migration
  def change
    create_table :aga_actions do |t|
      t.belongs_to :aga_challenge
      t.string :title
      t.string :slug
      t.text :description
      t.decimal :percentage, null: false, default: 0, precision: 6, scale: 3
      t.string :institutions
      t.string :managers
      t.date :deadline
      t.timestamps
    end
  end
end
