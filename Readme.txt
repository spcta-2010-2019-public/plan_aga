Instalar y configurar Git
Instalar Ruby en su computadora, utilizando guia de https://rvm.io/
**Nota: Instalar RVM 1.9.3 (comando: \curl -sSL https://get.rvm.io | bash -s stable --ruby=1.9.3)

En consola, verificar dependencias (comando: "rvm rquirements") e instalarlas.

Modificar archivo hosts (En linux debian/ubuntu: sudo nano /etc/hosts en linux) para admitir las siguientes rutas de URL:
Ejemplo:
127.0.0.1       localhost
127.0.1.1       (nombre del equipo)
127.0.0.1       admin.localhost.com
127.0.0.1       www.localhost.com
127.0.0.1       alianza.localhost.com

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

Crear carpeta del proyecto y clonar el proyecto. (git clone ---)

En consola:
Ubicarse en la raiz del proyecto.
Correr comando "gem install bundler" Referencia: https://rvm.io/integration/bundler
Correr el comando "bundle install", para instalar las gemas que se utilizan para el proyecto.
Verificar archivo config/database.yml y escribir las credenciales necesarias para el gestor de base de datos (MySQL)

Correr el comando "rake db:create"
Correr el comando "rake db:migrate"
(Opcional) modificar archivo db/seeds.rb para creación del usuario de prueba
Correr comando "rake db:seed"
Correr comando "rails server" para levantar servidor local

En navegador digitar cualquiera de las siguientes direcciones:
* para acceder al panel
  http://admin.localhost.com:3000
* para acceder al sitio (Front)
  http://admin.localhost.com:3000
  http://localhost:3000
